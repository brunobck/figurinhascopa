﻿using FigurinhasCopa.Application.Interface;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Services;

namespace FigurinhasCopa.Application
{
    public class SwapAppService :
        AppServiceBase<Swap>, 
        ISwapAppService
    {
        private readonly ISwapService _swapService;

        public SwapAppService(ISwapService swapService) 
            : base(swapService)
        {
            _swapService = swapService;
        }
    }
}
