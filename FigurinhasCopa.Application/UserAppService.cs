﻿using FigurinhasCopa.Application.Interface;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Services;

namespace FigurinhasCopa.Application
{
    public class UserAppService :
        AppServiceBase<User>,
        IUserAppService
    {
        private readonly IUserService _userService;

        public UserAppService(IUserService userService)
            : base(userService)
        {
            _userService = userService;
        }
    }
}
