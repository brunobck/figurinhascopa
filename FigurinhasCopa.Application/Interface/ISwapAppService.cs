﻿using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Application.Interface
{
    public interface ISwapAppService
        : IAppServiceBase<Swap>
    {
    }
}
