﻿using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Application.Interface
{
    public interface IUserStickerAppService
        : IAppServiceBase<UserSticker>
    {
        UserSticker FindByUserIdAndStickerNumber(int userId, int stickerNumber);
    }
}
