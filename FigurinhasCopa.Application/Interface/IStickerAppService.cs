﻿using System.Collections.Generic;
using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Application.Interface
{
    public interface IStickerAppService 
        : IAppServiceBase<Sticker>
    {
        Sticker FindByNumber(int number);
    }
}
