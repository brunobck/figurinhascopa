﻿using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Application.Interface
{
    public interface IUserAppService
        : IAppServiceBase<User>
    {
    }
}
