﻿using FigurinhasCopa.Application.Interface;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Services;

namespace FigurinhasCopa.Application
{
    public class UserStickerAppService :
        AppServiceBase<UserSticker>,
        IUserStickerAppService
    {
        private readonly IUserStickerService _userStickerService;

        public UserStickerAppService(IUserStickerService userStickerService)
            : base(userStickerService)
        {
            _userStickerService = userStickerService;
        }

        public UserSticker FindByUserIdAndStickerNumber(int userId, int stickerNumber)
        {
            return _userStickerService.FindByUserIdAndStickerNumber(userId, stickerNumber);
        }
    }
}
