﻿using System.Collections.Generic;
using FigurinhasCopa.Application.Interface;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Services;

namespace FigurinhasCopa.Application
{
    public class StickerAppService : AppServiceBase<Sticker>, IStickerAppService
    {
        private readonly IStickerService _stickerService;

        public StickerAppService(IStickerService stickerService)
            : base(stickerService)
        {
            _stickerService = stickerService;
        }

        public Sticker FindByNumber(int number)
        {
            return _stickerService.FindByNumber(number);
        }
    }
}
