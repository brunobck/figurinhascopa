﻿using System.ComponentModel.DataAnnotations;
using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.MVC.ViewModels
{
    public class RankingViewModel
    {
        public int StickerId { get; set; }

        public virtual Sticker Sticker { get; set; }

        [Display(Name = "Total")]
        public int Total { get; set; }
    }
}