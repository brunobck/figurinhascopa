﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.MVC.ViewModels
{
    public class UserStickerViewModel
    {
        [Key]
        public int UserStickerId { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }

        public int StickerId { get; set; }
        
        public virtual Sticker Sticker { get; set; }

        [Required(ErrorMessage = "Type a value")]
        [Range(typeof(int), "0", "9999999")]
        [Display(Name = "Quantidade")]
        public int Ammount { get; set; }
    }
}