﻿using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;

namespace FigurinhasCopa.MVC.ViewModels
{
    public class SwapViewModel
    {
        [Key]
        public int SwapId { get; set; }

        public int StickerId { get; set; }

        public virtual StickerViewModel Sticker { get; set; }

        [Required(ErrorMessage = "Type a value")]
        [Range(typeof(int), "0", "9999999")]
        [Display(Name = "Quantidade")]
        public int Ammount { get; set; }

        [Display(Name = "Pendente")]
        public bool IsPending { get; set; }

        public int UserFromId { get; set; }

        public virtual UserViewModel UserFrom { get; set; }

        public int UserToId { get; set; }

        public virtual UserViewModel UserTo { get; set; }
    }
}