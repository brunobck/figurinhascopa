﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FigurinhasCopa.MVC.ViewModels
{
    public class UserViewModel
    {
        [Key]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Fill the Name field")]
        [MaxLength(150, ErrorMessage = "Max {0} characters")]
        [MinLength(2, ErrorMessage = "Min {0} characters")]
        [DisplayName("Nome")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Fill the Email field")]
        [MaxLength(100, ErrorMessage = "Max {0} characters")]
        [MinLength(2, ErrorMessage = "Min {0} characters")]
        [EmailAddress(ErrorMessage = "Type a valid email")]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Fill the Name field")]
        [MaxLength(150, ErrorMessage = "Max {0} characters")]
        [MinLength(2, ErrorMessage = "Min {0} characters")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DisplayName("Senha")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public virtual IEnumerable<StickerViewModel> Stickers { get; set; }

    }
}