﻿using System.ComponentModel.DataAnnotations;

namespace FigurinhasCopa.MVC.ViewModels
{
    public class StickerViewModel
    {
        [Key]
        public int StickerId { get; set; }

        [Required(ErrorMessage = "Type a value")]
        [Display(Name = "Numero da figurinha")]
        [Range(typeof(int), "0", "9999999")]
        public int StickerNumber { get; set; }

        [Required(ErrorMessage = "Type a value")]
        [Display(Name = "Quantidade")]
        [Range(typeof(int), "0", "9999999")]
        public int Ammount { get; set; }
    }
}