﻿using AutoMapper;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.MVC.ViewModels;

namespace FigurinhasCopa.MVC.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<UserViewModel, User>();
            Mapper.CreateMap<StickerViewModel, Sticker>();
            Mapper.CreateMap<SwapViewModel, Swap>();
            Mapper.CreateMap<UserStickerViewModel, UserSticker>();
        }
    }
}