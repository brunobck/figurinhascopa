﻿using AutoMapper;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.MVC.ViewModels;

namespace FigurinhasCopa.MVC.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<User, UserViewModel>();
            Mapper.CreateMap<Sticker, StickerViewModel>();
            Mapper.CreateMap<Swap, SwapViewModel>();
            Mapper.CreateMap<UserSticker, UserStickerViewModel>();
        }
    }
}