﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FigurinhasCopa.Application.Interface;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.MVC.ViewModels;

namespace FigurinhasCopa.MVC.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private readonly IUserAppService _userAppService;

        public UsersController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        //
        // GET: /Users/
        public ActionResult Index()
        {
            var userViewModel = Mapper
                .Map<IEnumerable<User>, IEnumerable<UserViewModel>>(
                    _userAppService.GetAll());
            return View(userViewModel);
        }

        //
        // GET: /Users/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Users/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Users/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserViewModel userViewModel)
        {
            if (ModelState.IsValid)
            {
                var userDomain 
                    = Mapper.Map<UserViewModel, User>(userViewModel);
                _userAppService.Add(userDomain);

                LoginController loginController = new LoginController();
                loginController.Register(new RegisterViewModel()
                {
                    UserName = userDomain.Email,
                    Password = userDomain.Password,
                    ConfirmPassword = userDomain.Password
                });

                return RedirectToAction("Index");
            }

                return RedirectToAction("Index");
          
        }

        //
        // GET: /Users/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Users/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Users/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
