﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FigurinhasCopa.Application.Interface;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.MVC.ViewModels;

namespace FigurinhasCopa.MVC.Controllers
{
    [Authorize]
    public class StickersController : Controller
    {
        private readonly IStickerAppService _stickerAppService;
        private readonly IUserAppService _userAppService;
        private readonly ISwapAppService _swapAppService;
        private readonly IUserStickerAppService _userStickerAppService;

        public StickersController(
            IStickerAppService stickerAppService,
            IUserAppService userAppService,
            ISwapAppService swapAppService,
            IUserStickerAppService userStickerAppService)
        {
            _stickerAppService = stickerAppService;
            _userAppService = userAppService;
            _swapAppService = swapAppService;
            _userStickerAppService = userStickerAppService;
        }

        //
        // GET: /Stickers/
        public ActionResult Index()
        {
            var stickerViewModel = Mapper
                .Map<IEnumerable<Sticker>, IEnumerable<StickerViewModel>>
                    (_stickerAppService.GetAll());
            return View(stickerViewModel);
        }

        //
        // GET: /Stickers/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Stickers/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(
                _userAppService.GetAll(),
                "UserId",
                "Name");
            return View();
        }

        //
        // POST: /Stickers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StickerViewModel stickerViewModel)
        {
            //if (ModelState.IsValid)
            //{
            //    // tenta achar o sticker
            //    // caso ache, soma o valor na tabela Sticker e na UserSticker para o devido usuario
            //    // senão cria um novo Sticker e adiciona na UserSticker

            //    var stickerDomain = Mapper.Map<StickerViewModel, Sticker>
            //        (stickerViewModel);

            //    var sticker = _stickerAppService
            //        .FindByNumber(stickerViewModel.StickerNumber);

            //    if (sticker != null &&
            //        sticker.StickerNumber > 0)
            //    {
            //        sticker.Ammount += stickerDomain.Ammount;
            //        _stickerAppService.Update(sticker);

            //        // sticker já existia
            //        // atualizar na tabela de UserSticker
            //        var userSticker = _userStickerAppService
            //            .FindByUserIdAndStickerNumber(
            //                stickerViewModel.User.UserId,
            //                stickerViewModel.StickerNumber
            //            );

            //        if (userSticker != null &&
            //            userSticker.UserId > 0 &&
            //            userSticker.Sticker != null &&
            //            userSticker.Sticker.Ammount > 0)
            //        {
            //            // existe na tabela UserSticker

            //            userSticker.Ammount
            //                += userSticker.Ammount;
            //            _userStickerAppService.Update(userSticker);
            //        }
            //    }
            //    else
            //    {
            //        _stickerAppService.Add(stickerDomain);

            //        // salvar na tabela de UserSticker
            //        UserSticker userSticker = new UserSticker();
            //        userSticker.UserId = stickerViewModel.UserId;
            //        userSticker.StickerId = stickerViewModel.StickerId;
            //        userSticker.Ammount = stickerViewModel.Ammount;

            //        _userStickerAppService.Add(userSticker);
            //    }

            //    return RedirectToAction("Index");
            //}
            //ViewBag.UserId = new SelectList(
            //    _userAppService.GetAll(),
            //    "UserId",
            //    "Name");
            return View();
        }

        //
        // GET: /Stickers/Edit/5
        public ActionResult Edit(int id)
        {
            var stickerViewModel = Mapper
                .Map<Sticker, StickerViewModel>
                    (_stickerAppService.GetById(id));
            return View(stickerViewModel);
        }

        //
        // POST: /Stickers/Edit/5
        [HttpPost]
        public ActionResult Edit(StickerViewModel stickerViewModel)
        {
            if (ModelState.IsValid)
            {
                var stickerDomain = Mapper.Map<StickerViewModel, Sticker>
                    (stickerViewModel);
                _stickerAppService.Update(stickerDomain);

                return RedirectToAction("Index");
            }

            return View(stickerViewModel);
        }

        //
        // GET: /Stickers/Delete/5
        public ActionResult Delete(int id)
        {
            var sticker = _stickerAppService.GetById(id);
            var stickerViewModel = Mapper.Map<Sticker, StickerViewModel>(sticker);

            return View(stickerViewModel);
        }

        //
        // POST: /Stickers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var sticker = _stickerAppService.GetById(id);
            _stickerAppService.Remove(sticker);

            return RedirectToAction("Index");
        }
    }
}
