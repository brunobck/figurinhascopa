﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FigurinhasCopa.Application.Interface;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.MVC.ViewModels;

namespace FigurinhasCopa.MVC.Controllers
{
    [Authorize]
    public class UserStickerController : Controller
    {
        private readonly IStickerAppService _stickerAppService;
        private readonly IUserAppService _userAppService;
        private readonly ISwapAppService _swapAppService;
        private readonly IUserStickerAppService _userStickerAppService;

        public UserStickerController(
            IStickerAppService stickerAppService,
            IUserAppService userAppService,
            ISwapAppService swapAppService,
            IUserStickerAppService userStickerAppService)
        {
            _stickerAppService = stickerAppService;
            _userAppService = userAppService;
            _swapAppService = swapAppService;
            _userStickerAppService = userStickerAppService;
        }

        //
        // GET: /UserSticker/
        public ActionResult Index()
        {
            var userStickerViewModel = Mapper
                .Map<IEnumerable<UserSticker>, IEnumerable<UserStickerViewModel>>
                    (_userStickerAppService.GetAll().OrderBy(c => c.User.Name).ThenBy(c => c.StickerId));
            return View(userStickerViewModel);
        }

        //
        // GET: /UserSticker/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /UserSticker/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(
                _userAppService.GetAll(),
                "UserId",
                "Name");
            return View();
        }

        //
        // POST: /UserSticker/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserStickerViewModel userStickerViewModel)
        {
            if (ModelState.IsValid)
            {
                // tenta achar o sticker
                // caso ache, soma o valor na tabela Sticker e na UserSticker para o devido usuario
                // senão cria um novo Sticker e adiciona na UserSticker

                var userStickerDomain = Mapper.Map<UserStickerViewModel, UserSticker>
                    (userStickerViewModel);

                var sticker = _stickerAppService
                    .FindByNumber(userStickerDomain.Sticker.StickerNumber);

                if (sticker != null &&
                    sticker.StickerNumber > 0)
                {
                    sticker.Ammount += userStickerDomain.Ammount;
                    _stickerAppService.Update(sticker);

                    // sticker já existia
                    // atualizar na tabela de UserSticker
                    var userSticker = _userStickerAppService
                        .FindByUserIdAndStickerNumber(
                            userStickerDomain.UserId,
                            sticker.StickerNumber
                        );

                    if (userSticker != null &&
                        userSticker.UserId > 0 &&
                        userSticker.Sticker != null &&
                        userSticker.Sticker.Ammount > 0)
                    {
                        // existe na tabela UserSticker

                        userSticker.Ammount
                            += userStickerDomain.Ammount;
                        _userStickerAppService.Update(userSticker);
                    }
                    else
                    {
                        // salvar na tabela de UserSticker
                        UserSticker userSticker2 = new UserSticker();
                        userSticker2.UserId = userStickerDomain.UserId;
                        userSticker2.StickerId = sticker.StickerId;
                        userSticker2.Ammount = userStickerDomain.Ammount;

                        _userStickerAppService.Add(userSticker2);
                    }
                }
                else
                {
                    // salva sticker
                    var stickerObj = new Sticker();
                    stickerObj.StickerNumber = userStickerDomain.Sticker.StickerNumber;
                    stickerObj.Ammount = userStickerDomain.Ammount;
                    _stickerAppService.Add(stickerObj);

                    // salvar na tabela de UserSticker
                    UserSticker userSticker = new UserSticker();
                    userSticker.UserId = userStickerDomain.UserId;
                    userSticker.StickerId = stickerObj.StickerId;
                    userSticker.Ammount = userStickerDomain.Ammount;

                    _userStickerAppService.Add(userSticker);
                }

                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(
                _userAppService.GetAll(),
                "UserId",
                "Name");
            return View();
        }

        //
        // GET: /UserSticker/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /UserSticker/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /UserSticker/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /UserSticker/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
