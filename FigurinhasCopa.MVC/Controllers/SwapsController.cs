﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using FigurinhasCopa.Application.Interface;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.MVC.ViewModels;

namespace FigurinhasCopa.MVC.Controllers
{
    [Authorize]
    public class SwapsController : Controller
    {
        private readonly ISwapAppService _swapAppService;
        private readonly IStickerAppService _stickerAppService;
        private readonly IUserAppService _userAppService;
        private readonly IUserStickerAppService _userStickerAppService;

        public SwapsController(
            ISwapAppService swapAppService,
            IStickerAppService stickerAppService,
            IUserAppService userAppService,
            IUserStickerAppService userStickerAppService)
        {
            _swapAppService = swapAppService;
            _stickerAppService = stickerAppService;
            _userAppService = userAppService;
            _userStickerAppService = userStickerAppService;
        }

        //
        // GET: /Swaps/
        public ActionResult Index()
        {
            var swapViewModel = Mapper
                .Map<IEnumerable<Swap>, IEnumerable<SwapViewModel>>(
                    _swapAppService.GetAll());
            return View(swapViewModel);
        }

        //
        // GET: /Swaps/Ranking
        public ActionResult Ranking()
        {
            var swapViewModel = Mapper
                .Map<IEnumerable<Swap>, IEnumerable<SwapViewModel>>(
                    _swapAppService.GetAll());

            var swaps = _swapAppService.GetAll().Where(c => !c.IsPending);
            var sticker = _stickerAppService.GetAll();

            var swapsGroup = swaps.GroupBy(c => c.SwapId).Select(c => new
            {
                StickerId = c.Select(p => p.Sticker.StickerId).FirstOrDefault(),
                Total = c.Sum(a => a.Ammount)
            });

            var swapsGroupCount = swapsGroup.GroupBy(c => c.StickerId).Select(c => new
            {
                StickerId = c.Key,
                Total = c.Count()
            });

            var rankingViewModelList = new List<RankingViewModel>();
            RankingViewModel rankingViewModel = null;

            foreach (var userSticker in swapsGroupCount)
            {
                rankingViewModel = new RankingViewModel();
                rankingViewModel.StickerId = userSticker.StickerId;
                rankingViewModel.Sticker = new Sticker();
                rankingViewModel.Sticker = sticker
                    .Where(c => c.StickerId == userSticker.StickerId)
                    .FirstOrDefault();
                rankingViewModel.Total = userSticker.Total;

                rankingViewModelList.Add(rankingViewModel);
            }

            rankingViewModelList = rankingViewModelList
                .OrderByDescending(c => c.Total)
                .ToList();

            return View(rankingViewModelList);
        }


        //
        // GET: /Swaps/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Swaps/Create
        public ActionResult Create()
        {
            ViewBag.StickerId
                = new SelectList(_stickerAppService.GetAll(), "StickerId", "StickerNumber");
            ViewBag.UserFromId
                = new SelectList(_userAppService.GetAll(), "UserId", "Name");
            ViewBag.UserToId
                = new SelectList(_userAppService.GetAll(), "UserId", "Name");
            return View();
        }

        //
        // POST: /Swaps/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SwapViewModel swapViewModel)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Error = null;
                ViewBag.StickerId
                    = new SelectList(_stickerAppService.GetAll(), "StickerId", "StickerNumber");
                ViewBag.UserFromId
                    = new SelectList(_userAppService.GetAll(), "UserId", "Name");
                ViewBag.UserToId
                    = new SelectList(_userAppService.GetAll(), "UserId", "Name");

                var swapDomain
                    = Mapper.Map<SwapViewModel, Swap>(swapViewModel);
                swapDomain.IsPending = true;

                // verificar se o usuario não está transferindo para ele mesmo
                if (swapDomain.UserFromId == swapDomain.UserToId)
                {
                    ViewBag.Error = "Você não pode trocar figurinhas consigo mesmo";
                    return View();
                }

                // verificar se usuario tem quantidade de figurinhas
                // suficientes para trocar

                var sticker = _stickerAppService
                    .GetById(swapDomain.StickerId);

                var userStickerFrom = _userStickerAppService
                    .FindByUserIdAndStickerNumber(
                        swapDomain.UserFromId,
                        sticker.StickerNumber
                    );

                if (userStickerFrom != null && 
                    userStickerFrom.Ammount > 0 &&
                    userStickerFrom.Ammount >= swapDomain.Ammount)
                {
                    _swapAppService.Add(swapDomain);
                }
                else
                {
                    ViewBag.Error = "Usuário não contém figurinhas suficiente para realizar a troca";
                    return View();
                }
            }

            return RedirectToAction("Index");
        }

        //
        // Get: /Swaps/Confirm
        public ActionResult Confirm(int id)
        {
            if (ModelState.IsValid)
            {
                var swapDomain
                    = _swapAppService.GetById(id);

                if (swapDomain == null)
                    return RedirectToAction("Index");

                var sticker = _stickerAppService
                    .GetById(swapDomain.StickerId);

                // debit the stickers
                var userStickerFrom = _userStickerAppService
                    .FindByUserIdAndStickerNumber(
                        swapDomain.UserFromId,
                        sticker.StickerNumber
                    );

                userStickerFrom.Ammount -= swapDomain.Ammount;
                _userStickerAppService.Update(userStickerFrom);

                // credit the stickers
                var userStickerTo = _userStickerAppService
                    .FindByUserIdAndStickerNumber(
                        swapDomain.UserToId,
                        sticker.StickerNumber
                    );

                if (userStickerTo != null &&
                    userStickerTo.UserStickerId > 0)
                {
                    userStickerTo.Ammount += swapDomain.Ammount;
                    _userStickerAppService.Update(userStickerTo);
                }
                else
                {
                    // salvar na tabela de UserSticker
                    UserSticker userSticker2 = new UserSticker();
                    userSticker2.UserId = swapDomain.UserToId;
                    userSticker2.StickerId = sticker.StickerId;
                    userSticker2.Ammount = swapDomain.Ammount;

                    _userStickerAppService.Add(userSticker2);
                }

                swapDomain.IsPending = false;
                // salvar na tabela swap
                _swapAppService.Update(swapDomain);

                return RedirectToAction("Index");
            }
            ViewBag.StickerId
                = new SelectList(_stickerAppService.GetAll(), "StickerId", "StickerNumber");
            ViewBag.UserFromId
                = new SelectList(_userAppService.GetAll(), "UserId", "Name");
            ViewBag.UserToId
                = new SelectList(_userAppService.GetAll(), "UserId", "Name");
            return RedirectToAction("Index");
        }

        //
        // GET: /Swaps/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Swaps/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Swaps/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Swaps/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
