﻿using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Domain.Interfaces.Repositories
{
    public interface IUserRepository
        : IRepositoryBase<User>
    {
    }
}
