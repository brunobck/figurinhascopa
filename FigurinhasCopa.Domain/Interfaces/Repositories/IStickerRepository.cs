﻿using System.Collections.Generic;
using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Domain.Interfaces.Repositories
{
   public interface IStickerRepository
       : IRepositoryBase<Sticker>
   {
       Sticker FindByNumber(int number);
   }
}
