﻿using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Domain.Interfaces.Repositories
{
    public interface IUserStickerRepository
        : IRepositoryBase<UserSticker>
    {
        UserSticker FindByUserIdAndStickerNumber(int userId, int stickerNumber);
    }
}
