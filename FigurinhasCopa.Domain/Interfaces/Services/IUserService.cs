﻿using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Domain.Interfaces.Services
{
    public interface IUserService
        : IServiceBase<User>
    {

    }
}
