﻿using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Domain.Interfaces.Services
{
    public interface IUserStickerService
        : IServiceBase<UserSticker>
    {
        UserSticker FindByUserIdAndStickerNumber(int userId, int stickerNumber);
    }
}
