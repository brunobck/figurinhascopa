﻿using System.Collections.Generic;
using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Domain.Interfaces.Services
{
    public interface IStickerService
        : IServiceBase<Sticker>
    {
        Sticker FindByNumber(int number);
    }
}
