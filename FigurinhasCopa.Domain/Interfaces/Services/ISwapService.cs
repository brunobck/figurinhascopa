﻿using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Domain.Interfaces.Services
{
    public interface ISwapService
        : IServiceBase<Swap>
    {
    }
}
