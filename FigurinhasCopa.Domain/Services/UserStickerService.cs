﻿using System.Collections.Generic;
using System.Linq;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Repositories;
using FigurinhasCopa.Domain.Interfaces.Services;

namespace FigurinhasCopa.Domain.Services
{
    public class UserStickerService :
           ServiceBase<UserSticker>,
           IUserStickerService
    {
        private readonly IUserStickerRepository _userStickerRepository;

        public UserStickerService(IUserStickerRepository userStickerRepository)
            : base(userStickerRepository)
        {
            _userStickerRepository = userStickerRepository;
        }

        public UserSticker FindByUserIdAndStickerNumber(int userId, int stickerNumber)
        {
            return _userStickerRepository.FindByUserIdAndStickerNumber(userId, stickerNumber);
        }
    }
}
