﻿using System.Collections.Generic;
using System.Linq;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Repositories;
using FigurinhasCopa.Domain.Interfaces.Services;

namespace FigurinhasCopa.Domain.Services
{
    public class UserService :
           ServiceBase<User>,
           IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository UserUserRepository)
            : base(UserUserRepository)
        {
            _userRepository = UserUserRepository;
        }
    }
}
