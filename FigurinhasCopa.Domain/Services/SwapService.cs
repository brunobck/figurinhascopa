﻿using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Repositories;
using FigurinhasCopa.Domain.Interfaces.Services;

namespace FigurinhasCopa.Domain.Services
{
    public class SwapService :
        ServiceBase<Swap>,
        ISwapService
    {
        private readonly ISwapRepository _swapRepository;

        public SwapService(ISwapRepository swapRepository)
            : base(swapRepository)
        {
            _swapRepository = swapRepository;
        }
    }
}
