﻿using System.Collections.Generic;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Repositories;
using FigurinhasCopa.Domain.Interfaces.Services;

namespace FigurinhasCopa.Domain.Services
{
    public class StickerService :
        ServiceBase<Sticker>,
        IStickerService
    {
        private readonly IStickerRepository _stickerRepository;

        public StickerService(IStickerRepository stickerRepository)
            : base(stickerRepository)
        {
            _stickerRepository = stickerRepository;
        }

        public Sticker FindByNumber(int number)
        {
            return _stickerRepository.FindByNumber(number);
        }
    }
}
