﻿using System.Collections.Generic;

namespace FigurinhasCopa.Domain.Entities
{
    public class User
    {
        public int UserId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public virtual IEnumerable<Sticker> Stickers { get; set; }
    }
}
