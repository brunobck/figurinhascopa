﻿
namespace FigurinhasCopa.Domain.Entities
{
    public class Sticker
    {
        public int StickerId { get; set; }

        public int StickerNumber { get; set; }

        public int Ammount { get; set; }
    }
}
