﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FigurinhasCopa.Domain.Entities
{
    public class Swap
    {
        public int SwapId { get; set; }

        public int StickerId { get; set; }

        public virtual Sticker Sticker { get; set; }

        public int Ammount { get; set; }

        public bool IsPending { get; set; }

        public int UserFromId { get; set; }

        public virtual User UserFrom { get; set; }

        public int UserToId { get; set; }

        public virtual User UserTo { get; set; }
    }
}
