﻿using System.Collections.Generic;

namespace FigurinhasCopa.Domain.Entities
{
    public class UserSticker
    {
        public int UserStickerId { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }

        public int StickerId { get; set; }

        public virtual Sticker Sticker { get; set; }

        public int Ammount { get; set; }
    }
}
