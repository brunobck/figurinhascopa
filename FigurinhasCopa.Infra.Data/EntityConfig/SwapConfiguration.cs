﻿using System.Data.Entity.ModelConfiguration;
using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Infra.Data.EntityConfig
{
    public class SwapConfiguration
        : EntityTypeConfiguration<Swap>
    {
        public SwapConfiguration()
        {
            HasKey(c => c.SwapId);

            Property(c => c.Ammount)
                .IsRequired();

            Property(c => c.IsPending)
                .IsRequired();

            HasRequired(p => p.Sticker)
                .WithMany()
                .HasForeignKey(p => p.StickerId);

            HasRequired(p => p.UserFrom)
                .WithMany()
                .HasForeignKey(p => p.UserFromId);

            HasRequired(p => p.UserTo)
                .WithMany()
                .HasForeignKey(p => p.UserToId);

        }
    }
}
