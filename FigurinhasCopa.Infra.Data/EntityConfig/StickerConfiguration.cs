﻿using System.Data.Entity.ModelConfiguration;
using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Infra.Data.EntityConfig
{
    public class StickerConfiguration
        : EntityTypeConfiguration<Sticker>
    {
        public StickerConfiguration()
        {
            HasKey(c => c.StickerId);

            Property(c => c.StickerNumber)
                .IsRequired();

            Property(c => c.Ammount)
                .IsRequired();
        }
    }
}
