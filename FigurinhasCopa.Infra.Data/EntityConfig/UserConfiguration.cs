﻿using System.Data.Entity.ModelConfiguration;
using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Infra.Data.EntityConfig
{
    public class UserConfiguration
        : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            HasKey(c => c.UserId);

            Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(150);

            Property(c => c.Email)
                .IsRequired()
                .HasMaxLength(150);

            Property(c => c.Password)
                .HasMaxLength(150);
        }
    }
}
