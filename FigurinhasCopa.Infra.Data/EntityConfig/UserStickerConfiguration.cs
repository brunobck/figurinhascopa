﻿using System.Data.Entity.ModelConfiguration;
using FigurinhasCopa.Domain.Entities;

namespace FigurinhasCopa.Infra.Data.EntityConfig
{
    public class UserStickerConfiguration
        : EntityTypeConfiguration<UserSticker>
    {
        public UserStickerConfiguration()
        {
            HasKey(c => c.UserStickerId);

            Property(p => p.Ammount)
                .IsRequired();

            HasRequired(c => c.Sticker)
                .WithMany()
                .HasForeignKey(c => c.StickerId);

            HasRequired(c => c.User)
                .WithMany()
                .HasForeignKey(c => c.UserId);
        }
    }
}
