﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Infra.Data.EntityConfig;

namespace FigurinhasCopa.Infra.Data.Context
{
    public class FigurinhasCopaContext : DbContext
    {
        public FigurinhasCopaContext()
            : base("FigurinhasCopa")
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Sticker> Stickers { get; set; }
        public DbSet<Swap> Swaps { get; set; }
        public DbSet<UserSticker> UserStickers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions
                .Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions
                .Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions
                .Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties()
                .Where(p => p.Name.Equals(p.ReflectedType.Name + "Id"))
                .Configure(p => p.IsKey());

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(100));

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new StickerConfiguration());
            modelBuilder.Configurations.Add(new SwapConfiguration());
            modelBuilder.Configurations.Add(new UserStickerConfiguration());
        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries()
                .Where(entry => entry.Entity
                                    .GetType()
                                    .GetProperty("DtRegister") != null))
            {
                if (entry.State == EntityState.Added)
                    entry.Property("DtRegister").CurrentValue = DateTime.Now;

                if (entry.State == EntityState.Modified)
                    entry.Property("DtRegister").IsModified = false;
            }

            return base.SaveChanges();
        }
    }
}
