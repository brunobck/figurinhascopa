﻿using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Repositories;

namespace FigurinhasCopa.Infra.Data.Repositories
{
    public class SwapRepository
        : RepositoryBase<Swap>,
            ISwapRepository
    {
    }
}
