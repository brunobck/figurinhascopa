﻿using System.Linq;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Repositories;

namespace FigurinhasCopa.Infra.Data.Repositories
{
    public class UserRepository
        : RepositoryBase<User>,
            IUserRepository
    {

    }
}
