﻿using System.Collections.Generic;
using System.Linq;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Repositories;

namespace FigurinhasCopa.Infra.Data.Repositories
{
    public class StickerRepository
        : RepositoryBase<Sticker>,
            IStickerRepository
    {
        public Sticker FindByNumber(int number)
        {
            return Db.Stickers
                .Where(c => c.StickerNumber == number)
                .FirstOrDefault();
        }
    }
}
