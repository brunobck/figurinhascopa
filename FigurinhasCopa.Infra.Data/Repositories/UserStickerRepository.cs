﻿using System.Linq;
using FigurinhasCopa.Domain.Entities;
using FigurinhasCopa.Domain.Interfaces.Repositories;

namespace FigurinhasCopa.Infra.Data.Repositories
{
    public class UserStickerRepository
        : RepositoryBase<UserSticker>,
            IUserStickerRepository
    {
        public UserSticker FindByUserIdAndStickerNumber(int userId, int stickerNumber)
        {
            var sticker = Db.Stickers
                .Where(c => c.StickerNumber == stickerNumber)
                .FirstOrDefault();

            return Db.UserStickers
                .Where(c => c.UserId == userId
                            && c.StickerId == sticker.StickerId)
                .FirstOrDefault();
        }
    }
}
